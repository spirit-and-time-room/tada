<?php

class Database { 
  public $mysqli;
  public $count;

  function __construct() {
    // db接続
    $this->mysqli = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    if ($this->mysqli->connect_error) {
      echo $this->mysqli->connect_error;
      exit();
    } else {
      $this->mysqli->set_charset("utf8");
    }
  }

  function __destruct() {
    // db切断
    $this->mysqli->close();
  }

  function query($sql) {
    // sql実行
    $result = $this->mysqli->query($sql);

    // エラー処理
    if ($result == FALSE) {
      $error = $this->mysqli->errno.": ".$this->mysqli->error;
      return json_array(FALSE, 0, "", $error);
    }
    
    if ($result === TRUE) {
      $this->count = $this->mysqli->affected_rows;
      return json_array(TRUE, $this->count, "", "");
    } else {
      $this->count = $result->num_rows;
      $data = array();
      while ($row = $result->fetch_assoc()) {
        $data[] = $row;
      }
      $result->close();
      return $this->json_array(TRUE, $this->count, $data, "");
    }
  }

  function json_array($status, $count, $result, $error) {
    $rtn = array(
      'status' => $status,
      'count'  => $count,
      'result' => $result,
      'error'  => $error
    );
    return json_encode($rtn);
  }
}






